﻿using as1Tuan;

class Program
{
    static void Main()
    {
        int n = 0;
        bool nhapThanhCong = false;

        while (!nhapThanhCong)
        {
            Console.Write("Nhap so luong giao vien : ");
            string input = Console.ReadLine();

            if (int.TryParse(input, out n) && n > 0)
            {
                nhapThanhCong = true;
            }
            else
            {
                Console.WriteLine("Nhap khong hop le. Xin vui long nhap mot so nguyen duong.");
            }
        }

        Console.WriteLine($"Ban da nhap so luong giao vien la: {n}");


        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"Nhap thong tin giao vien {i + 1}:");
            
            
            string hoTen = "";
            bool NhapLaiten = true;
            while (NhapLaiten)
            {
                Console.Write("HO Ten: ");
                try
                {
                    hoTen = Console.ReadLine();
                    if( hoTen !=null&& hoTen != "" && !hoTen.Any(char.IsDigit))
                    {
                       
                        NhapLaiten = false;
                    }
                    else
                    {
                        Console.WriteLine("khong hop le.");
                        NhapLaiten = true;
                    }

                }
                catch(FormatException) {
                    Console.WriteLine("Nhap khong hop le. Xin vui long nhap lai.");
                }
            }

           
          

            int namSinh = 0;
            bool NhapLai = true;

            while (NhapLai)
            {
                Console.Write("Nam sinh: ");
                try
                {
                    namSinh = int.Parse(Console.ReadLine());

                    // Kiểm tra xem năm sinh có nằm trong một khoảng cụ thể hay không
                    if (namSinh >= 1900 && namSinh <= 2003)
                    {
                        NhapLai = false;
                    }
                    else
                    {
                        Console.WriteLine("Nam sinh khong hop le.");
                        NhapLai = true;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Nhap khong hop le. Xin vui long nhap lai.");
                }
            }


            double luongCoBan = 0.0;
            bool nhapLaiLuong = true;

            while (nhapLaiLuong)
            {
                Console.Write("Luong co ban: ");
                try
                {
                    luongCoBan = double.Parse(Console.ReadLine());


                    if (luongCoBan != 0)
                    {
                        nhapLaiLuong = false;
                    }
                    else
                    {
                        Console.WriteLine("Luong khong hop le.");
                        nhapLaiLuong = true;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Nhap khong hop le. Xin vui long nhap lai.");
                }
            }


            double heSoLuong = 0.0;
            bool NhapLaiHeSo = true;

            while (NhapLaiHeSo)
            {
                Console.Write("He so: ");
                try
                {
                    heSoLuong = double.Parse(Console.ReadLine());

                    
                    if (heSoLuong !=0)
                    {
                        NhapLaiHeSo = false;
                    }
                    else
                    {
                        Console.WriteLine("He so khong hop le.");
                        NhapLaiHeSo = true;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Nhap khong hop le. Xin vui long nhap lai.");
                }
            }
            

            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
        }

        // Tìm giáo viên có lương thấp nhất
        GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];
        foreach (var giaoVien in danhSachGiaoVien)
        {
            if (giaoVien.TinhLuong() < giaoVienLuongThapNhat.TinhLuong())
            {
                giaoVienLuongThapNhat = giaoVien;
            }
        }

        Console.WriteLine("Thong tin giao vien co luong thap nhat:");
        giaoVienLuongThapNhat.XuatThongTin();

        // Dừng màn hình để xem kết quả
        Console.WriteLine("Enter de ket thuc chuong trinh");
        Console.ReadLine();
    }
}