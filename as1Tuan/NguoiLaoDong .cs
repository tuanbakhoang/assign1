﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace as1Tuan
{

    public class NguoiLaoDong
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }

        // Hàm khởi tạo mặc định
        public NguoiLaoDong()
        {
           
        }

        // Hàm khởi tạo với tham số
        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        // Hàm nhập thông tin
        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        // Hàm tính lương
        public double TinhLuong()
        {
            return LuongCoBan;
        }

        // Hàm xuất thông tin
        public void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
        }
    }

 

}
