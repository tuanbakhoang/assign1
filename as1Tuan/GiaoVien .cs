﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace as1Tuan
{
    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien()
        {
            HeSoLuong = 0.0;
        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
            : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }

        public new double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}
